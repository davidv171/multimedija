
/*V sklopu 1. domače naloge s poljubnim programskim jezikom implementirate konzolno aplikacijo za osnovno urejanje binarnih datotek. Aplikacija naj omogoča naslednje osnovne I/O operacije:



- Binarni zapis ali branje zaporedja bitov (npr. write_bits(std::vector<bool> &bits){...} ali std::vector<bool> read_bits(const int &n){..}).

- Binarni zapis ali branje podatkovnih tipov 32-bitni int, 32-bitni float in 8-bitni char.



Na podlagi zgornjih operacij implementirate naslednje funkcionalnosti:


- Iskanje zaporedja bitov (brez prepletanja) v vhodni binarni datoteki ter izpis pozicije.

- Zamenjava iskanih bitov v vhodni binarni datoteki (ti. find+replace all)



Vhod/izhod konzolne aplikacije (preko argumentov)

dn1 <vhodna datoteka> <opcija> <podatek1> <podatek2>

kjer:

<opcija>:
f - operacija iskanja bitov iz <podatek1>
fr - operacija zamenjava bitov iz <podatek1> z biti iz <podatek2>
<podatek1> in <podatek2> - zaporedje bitov
<vhodna datoteka> - pot do poljubne datoteke, ki jo odprete v binarnem načinu.
Primer uporabe:

dn1 test.bin f 0110100001100101011011000110110001101111

dn1 test.bin fr 0000000 1111*/
import java.util.*;
import java.awt.print.PrinterAbortException;
import java.io.*;

class BitOperations {
	// Turn a string into an arraylist of booleans
	public static ArrayList<Boolean> stringToBits(String string) {
		ArrayList<Boolean> bitArray = new ArrayList<>();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '1') {
				bitArray.add(true);
			} else {
				bitArray.add(false);
			}
		}
		return bitArray;
	}

	public static ArrayList<Boolean> byteToBits(int currentByte) {
		ArrayList<Boolean> bits = new ArrayList<>();
		for (int i = 7; i >= 0; i--) {
			if (currentByte << ~i < 0) {
				bits.add(true);
				//System.out.print(1);
			} else {
				bits.add(false);
				//System.out.print(0);
			}
		}
		return bits;
	}

	public static byte[] bitsToByte(ArrayList<Boolean> writeArr) throws ArrayIndexOutOfBoundsException {
		byte[] bytes = new byte[writeArr.size() / 8];
		for (int i = 0; i < writeArr.size() / 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (writeArr.get(i * 8 + j)) {
					bytes[i] |= (128 >> j);
				}

			}
		}
		return bytes;
	}

	public static ArrayList<Boolean> replaceBits(String search, String replace, ArrayList<Boolean> bitArr,
			ArrayList<Boolean> replaceArr, ArrayList<Boolean> searchArr) {
		ArrayList<Boolean> writeArr = new ArrayList<>();
		writeArr.addAll(bitArr);
		String bigString = writeArr.toString();
		int delCounter = 0;
		int addCounter = 0;
		// Counter for each and every added element we get, will be 0 if replace string
		// isnt larger than search string
		// Counter for each and every removed element we get, will be 0 if replace
		// string is larger than search string
		// j = 0 -> replace length
		System.out.println("Replacing...");
		try {
			for (int i = 0; i < writeArr.size(); i+=search.length()) {
				if (writeArr.subList(i, i + search.length()).equals(searchArr)) {
					// Ko dobiš match dodaj replacement bite namesto navadnih
					for (int j = 0; j < replace.length(); j++) {
						if (replace.length() == search.length()) {
							writeArr.set(i + j, replaceArr.get(j));
							System.out.println("Writing on " + (i+j) + replaceArr.get(j));
						}
						// če je replace string daljši kot naš search string pomeni, da moramo polek za
						// menjanih indeksov na konec še dodati ostanek, torej: fr 0101 0110010101
						if (replace.length() > search.length()) {
							if (j < search.length()) {
								writeArr.set(i + j + addCounter, replaceArr.get(j));
							} else {
								// Dodaj nove vrednosti na konec
								writeArr.add(i+ j, replaceArr.get(j));
								if(j == replace.length()-1){
									i+=8;
								}
								System.out.println("Added" + replaceArr.get(j) + " on " + (i+j+addCounter));
							}
						}
						// če je replace string krajši kot naš search moramo odstraniti razliko v ve
						// ikosti
						// fr 0101 00 -> 00 in odstrani 01
						if (replace.length() < search.length()) {
							System.out.println("ij " + writeArr.get(i+j));
							int diff = search.length() - replace.length();
							// Remove until search size
							writeArr.set(i + j, replaceArr.get(j));
							if (j == replace.length()-1) {
								for (int z = 0; z < diff; z++) {
									// Diff krat izbriši zadnjega
									writeArr.remove((i + j));
									System.out.println("Removing" + (i+j));
									i--;
								}
							}

						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Search size" + search.length());
			return writeArr;

		}
		return writeArr;

	}

	public static void main(String[] args) {
		String filepath = args[0];
		String operation = args[1];
		String search = args[2];
		if (filepath == null || operation == null || search == null) {
			System.out.println("Incorrect usage");
			System.exit(1);
		}
		if (operation.equals("f")) {
			ArrayList<Boolean> bitArr = new ArrayList<>();
			ArrayList<Boolean> searchArr = new ArrayList<>();
			DataInputStream inputStream = null;
			try {
				inputStream = new DataInputStream(new FileInputStream(filepath));
				int currentByte;
				// String to bitset
				searchArr = stringToBits(search);
				while (true) {
					currentByte = inputStream.readUnsignedByte();
					bitArr.addAll(byteToBits(currentByte));
				}

			} catch (EOFException e) {
				try {
					inputStream.close();
					// Pridobi bajte
					System.out.println("\nFinding matches.....");
					for (int i = 0; i < bitArr.size(); i+=search.length()) {
						if (bitArr.subList(i, i + search.length()).equals(searchArr)) {
							System.out.println("Index matches on: " + i + " until " + (i + search.length()));
						}
					}
				} catch (FileNotFoundException exc) {
					System.out.println("File not found");

					System.exit(1);
				} catch (IOException exception) {
					System.out.println("IO");

					System.exit(1);
				} catch (IndexOutOfBoundsException ex) {
					System.exit(1);
				}
			} catch (FileNotFoundException ex) {
				System.out.println("File not found");
				System.exit(1);
			} catch (IOException exception) {
				System.exit(1);
			}
		}
		if (operation.equals("fr")) {
			String replace = args[3];
			ArrayList<Boolean> bitArr = new ArrayList<>();
			ArrayList<Boolean> searchArr = new ArrayList<>();
			ArrayList<Boolean> replaceArr = new ArrayList<>();
			ArrayList<Boolean> writeArr = new ArrayList<>();
			DataOutputStream outputStream = null;
			DataInputStream inputStream = null;
			try {
				inputStream = new DataInputStream(new FileInputStream(filepath));
				int currentByte;
				searchArr = stringToBits(search);
				replaceArr = stringToBits(replace);
				while (true) {
					currentByte = inputStream.readUnsignedByte();
					bitArr.addAll(byteToBits(currentByte));
				}

			} catch (EOFException e) {
				try {
					inputStream.close();
					outputStream = new DataOutputStream(new FileOutputStream("userout.bmp"));
					// Pridobi bajte
					writeArr = replaceBits(search, replace, bitArr, replaceArr, searchArr);
					byte[] result = bitsToByte(writeArr);
					outputStream.write(result);
					outputStream.close();
				} catch (FileNotFoundException exc) {
					System.out.println("Not found file");
					System.exit(1);
				} catch (IOException exception) {
					System.out.println("IO?");
					System.exit(1);
				} catch (IndexOutOfBoundsException ex) {
					System.out.println("Arrays");
					System.exit(1);
				}
			} catch (FileNotFoundException ex) {
				System.exit(1);
			} catch (IOException exception) {
				System.exit(1);
			}
		}

	}

}
